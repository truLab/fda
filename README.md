# FDA drug record applications service

Application built with SpringBoot for the backend solution and mongoDB for data storage.  
Solution is dockerized.

## Build and run the project
* Building application with tests from command line:
```
mvnw clean install
```

* Running application

Both backend and mongo could be started with docker-compose.  
Run from the root:
```
docker-compose up
```

## How to use
 
[Swagger-ui](localhost:8080/swagger-ui-fda)  

[Swagger-doc](localhost:8080/fda-api-docs)

There are two main sections defined:  
* drug record application that uses local storage. Enables to save and find data with mongoDB
  * Add new drug record application
  ```
  [POST] /api/storage/drug-record-application
  ```
  * Find one record by application number
  ```
  [GET] /api/storage/drug-record-application/{applicationNumber}
  ```
  * Find records with a standard paging
  ```
  [GET] /api/storage/drug-record-application/by-page
  ```
  * Find records with a cursor paging
  ```
  [GET] /api/storage/drug-record-application/by-cursor
  ```
* fda search which allows querying FDA open api
  * Find records in fda open api
  ```
  [GET] /api/fda/drug-record-application
  ```
