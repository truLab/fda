package com.interview.fda.search.controller;

import com.interview.fda.config.Fixture;
import com.interview.fda.config.IntegrationTests;
import com.interview.fda.search.client.FdaClient;
import com.interview.fda.search.service.FdaDrugRecordApplicationSearchService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class FdaSearchControllerTest extends IntegrationTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FdaClient fdaClient;

    @InjectMocks
    private FdaDrugRecordApplicationSearchService service;

    private final Fixture fixture = new Fixture();

    @Test
    public void shouldFindRecordWhenAllDataProvided() throws Exception {
        // given
        final String uri = "/api/fda/drug-record-application?" +
                "manufacturer_name=" + "manufacturerName" +
                "&brand_name=" + "brandName" +
                "&page=" + 0 +
                "&page_size=" + 2;

        Mockito.when(fdaClient.getDrugsFda(any()))
                .thenReturn(fixture.getFdaSearchResponse());

        // when then
        mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldReturnBadRequestWhenSizeIsZero() throws Exception {
        // given
        final String uri = "/api/fda/drug-record-application?" +
                "manufacturer_name=" + "manufacturerName" +
                "&brand_name=" + "brandName" +
                "&page=" + 1 +
                "&page_size=" + 0;

        Mockito.when(fdaClient.getDrugsFda(any()))
                .thenReturn(fixture.getFdaSearchResponse());

        // when then
        mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestWhenManufacturerNameNotProvided() throws Exception {
        // given
        final String uri = "/api/fda/drug-record-application?" +
                "&brand_name=" + "brandName" +
                "&page=" + 1 +
                "&page_size=" + 2;

        Mockito.when(fdaClient.getDrugsFda(any()))
                .thenReturn(fixture.getFdaSearchResponse());

        // when then
        mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFindRecordWhenBrandNameNotProvided() throws Exception {
        // given
        final String uri = "/api/fda/drug-record-application?" +
                "manufacturer_name=" + "manufacturerName" +
                "&page=" + 0 +
                "&page_size=" + 2;

        Mockito.when(fdaClient.getDrugsFda(any()))
                .thenReturn(fixture.getFdaSearchResponse());

        // when then
        mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

}
