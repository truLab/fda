package com.interview.fda.config;

import com.interview.fda.search.dto.client.FdaMeta;
import com.interview.fda.search.dto.client.FdaMetaResults;
import com.interview.fda.search.dto.client.FdaProduct;
import com.interview.fda.search.dto.client.FdaResult;
import com.interview.fda.search.dto.client.FdaSearchResponse;
import com.interview.fda.search.dto.client.OpenFda;
import com.interview.fda.storage.model.DrugRecordApplication;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Fixture {

    public DrugRecordApplication getSingleDrugRecordApplication() {
        return DrugRecordApplication.builder()
                .applicationNumber("app number")
                .manufacturerName("manuf name")
                .substanceName("subs name")
                .productNumbers(List.of("p1"))
                .build();
    }

    public List<DrugRecordApplication> getMultipleDrugRecordApplication() {
        return  IntStream.rangeClosed(1, 10).mapToObj(shift ->
            DrugRecordApplication.builder()
                    .applicationNumber("app_" + shift)
                    .manufacturerName("man_" + shift)
                    .substanceName("subs_" + shift)
                    .productNumbers(List.of("p1_" + shift, "p2_" + shift))
                    .build()
        ).collect(Collectors.toList());
    }

    public FdaSearchResponse getFdaSearchResponse() {
        FdaMetaResults metaResults = new FdaMetaResults(0, 2, 1);
        FdaMeta meta = new FdaMeta(metaResults);

        OpenFda openFda = new OpenFda(List.of("app_num"), List.of("b_name"), List.of("gen_n"), List.of("man_nam"));
        FdaResult result = new FdaResult("app_num", "spons_name", openFda, List.of(new FdaProduct("p1")));

        return new FdaSearchResponse(meta, List.of(result));
    }
}
