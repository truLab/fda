package com.interview.fda.storage.repository;

import com.interview.fda.common.model.Pagination;
import com.interview.fda.config.Fixture;
import com.interview.fda.config.IntegrationTests;
import com.interview.fda.storage.model.Cursor;
import com.interview.fda.storage.model.DrugRecordApplication;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class DrugRecordApplicationStorageRepositoryTest extends IntegrationTests {
    @Autowired
    private DrugRecordApplicationStorageRepository repository;

    private final Fixture fixture = new Fixture();

    @Test
    void shouldSaveDrugRecordApplication() {
        // given
        final DrugRecordApplication record = fixture.getSingleDrugRecordApplication();

        // when
        repository.save(record);

        // then
        final List<DrugRecordApplication> results = mongoTemplate.findAll(DrugRecordApplication.class);
        assertThat(results.size()).isEqualTo(1);
        assertResult(results.get(0), record);
    }

    @Test
    void shouldSaveMultipleDrugRecordApplications() {
        // given
        final DrugRecordApplication record1 = fixture.getSingleDrugRecordApplication();
        final DrugRecordApplication record2 = fixture.getSingleDrugRecordApplication();
        final DrugRecordApplication record3 = fixture.getSingleDrugRecordApplication();

        // when
        repository.save(record1);
        repository.save(record2);
        repository.save(record3);

        // then
        final List<DrugRecordApplication> results = mongoTemplate.findAll(DrugRecordApplication.class);
        assertThat(results.size()).isEqualTo(3);
    }

    @Test
    void shouldFindByApplicationNumber() {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        final String applicationNumber = records.get(0).getApplicationNumber();
        mongoTemplate.insertAll(records);

        // when
        final Optional<DrugRecordApplication> maybeResult = repository.findByApplicationNumber(applicationNumber);

        // then
        assertThat(maybeResult.get().getApplicationNumber()).isEqualTo(applicationNumber);
    }

    @Test
    void shouldNotFindByApplicationNumberWhenNothingStored() {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        final String applicationNumber = records.get(0).getApplicationNumber();

        // when
        final Optional<DrugRecordApplication> maybeResult = repository.findByApplicationNumber(applicationNumber);

        // then
        assertThat(maybeResult.isPresent()).isEqualTo(false);
    }

    @Test
    void shouldFindByPage() {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when
        final List<DrugRecordApplication> page0Results =
                repository.findByPage(Pagination.builder().page(0).pageSize(5).build());
        final List<DrugRecordApplication> page1Results =
                repository.findByPage(Pagination.builder().page(1).pageSize(5).build());
        final List<DrugRecordApplication> page2Results =
                repository.findByPage(Pagination.builder().page(2).pageSize(5).build());

        // then
        assertThat(page0Results.size()).isEqualTo(5);
        assertThat(page1Results.size()).isEqualTo(5);
        assertThat(page2Results.size()).isEqualTo(0);

        assertPages(records, page0Results, page1Results);
    }

    @Test
    void shouldFindByCursor() {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when
        final List<DrugRecordApplication> page0Results =
                repository.findByCursor(Cursor.builder().cursor(null).size(5).build());

        final List<DrugRecordApplication> page1Results =
                repository.findByCursor(Cursor.builder().cursor(getRecentCursor(page0Results)).size(5).build());

        // then
        assertThat(page0Results.size()).isEqualTo(5);
        assertThat(page1Results.size()).isEqualTo(5);

        assertPages(records, page0Results, page1Results);
    }

    private void assertResult(DrugRecordApplication result, DrugRecordApplication record) {
        assertThat(result.getApplicationNumber()).isEqualTo(record.getApplicationNumber());
        assertThat(result.getManufacturerName()).isEqualTo(record.getManufacturerName());
        assertThat(result.getSubstanceName()).isEqualTo(record.getSubstanceName());
        assertThat(result.getProductNumbers()).isEqualTo(record.getProductNumbers());
    }

    private ObjectId getRecentCursor(List<DrugRecordApplication> drugRecordApplications) {
        return drugRecordApplications.stream()
                .map(DrugRecordApplication::getId).max(ObjectId::compareTo)
                .orElse(null);
    }

    private void assertPages(List<DrugRecordApplication> records,
                             List<DrugRecordApplication> page0Results,
                             List<DrugRecordApplication> page1Results) {
        final List<String> page0AppNumbers =
                page0Results.stream().map(DrugRecordApplication::getApplicationNumber).collect(Collectors.toList());
        final List<String> page1AppNumbers =
                page1Results.stream().map(DrugRecordApplication::getApplicationNumber).collect(Collectors.toList());

        final long foundRecordsInPage0 =
                records.stream().map(DrugRecordApplication::getApplicationNumber).filter(page0AppNumbers::contains).count();
        final long foundRecordsInPage1 =
                records.stream().map(DrugRecordApplication::getApplicationNumber).filter(page1AppNumbers::contains).count();

        assertThat(foundRecordsInPage0).isEqualTo(5);
        assertThat(foundRecordsInPage1).isEqualTo(5);
    }
}
