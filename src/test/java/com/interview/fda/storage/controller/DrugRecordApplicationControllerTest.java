package com.interview.fda.storage.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.interview.fda.config.Fixture;
import com.interview.fda.config.IntegrationTests;
import com.interview.fda.storage.model.DrugRecordApplication;
import org.bson.types.ObjectId;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class DrugRecordApplicationControllerTest extends IntegrationTests {

    @Autowired
    private MockMvc mvc;

    private final Fixture fixture = new Fixture();

    @Test
    public void shouldSaveNewRecord() throws Exception {
        // given
        final DrugRecordApplication record = fixture.getSingleDrugRecordApplication();
        final String jsonRecord = getJsonBody(record);

        // when then
        mvc.perform(post("/api/storage/drug-record-application")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRecord))
                .andExpect(status().isOk());

        final List<DrugRecordApplication> savedRecords = mongoTemplate.findAll(DrugRecordApplication.class);
        assertThat(savedRecords.size()).isEqualTo(1);
        assertThat(savedRecords.get(0).getApplicationNumber()).isEqualTo(record.getApplicationNumber());
    }

    @Test
    public void shouldNotSaveRecordWithoutMandatoryField() throws Exception {
        // given
        final DrugRecordApplication record = DrugRecordApplication.builder().build();
        final String jsonRecord = getJsonBody(record);

        // when then
        mvc.perform(post("/api/storage/drug-record-application")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonRecord))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFindByApplicationNumber() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        final String applicationNumber = records.get(0).getApplicationNumber();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/" + applicationNumber)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.application_number", is(applicationNumber)))
                .andExpect(jsonPath("$.manufacturer_name", is(records.get(0).getManufacturerName())))
                .andExpect(jsonPath("$.substance_name", is(records.get(0).getSubstanceName())))
                .andExpect(jsonPath("$.product_numbers", is(records.get(0).getProductNumbers())));
    }

    @Test
    public void shouldReturnBadRequestWhenApplicationNumberNotProvided() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldNotFoundWhenRequestNonExistingRecord() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/" + "wrongId")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldFindByPageWithLinks() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/by-page?page=1&page_size=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$._links.nextPage.href", containsString("page=2&page_size=2")))
                .andExpect(jsonPath("$._links.previousPage.href", containsString("page=0&page_size=2")))
                .andExpect(jsonPath("$.records[0].application_number", containsString("app_")));
    }

    @Test
    public void shouldReturnBadRequestWhenPageIsNegative() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/by-page?page=-1&page_size=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestWhenSizeIsZero() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/by-page?page=1&page_size=0")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFindByCursorWithLinks() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/by-cursor?size=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.cursor", Matchers.notNullValue()))
                .andExpect(jsonPath("$._links.nextPage.href", containsString("cursor=")))
                .andExpect(jsonPath("$.records[0].application_number", containsString("app_")));
    }

    @Test
    public void shouldFindByCursorAndReturnEmptyCursorWhenLastResult() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);
        final List<DrugRecordApplication> allSavedDocs = mongoTemplate.findAll(DrugRecordApplication.class);
        final ObjectId recentCursor = getRecentCursor(allSavedDocs);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/by-cursor?cursor="+ recentCursor +"&size=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.cursor", Matchers.blankOrNullString()));
    }

    @Test
    public void shouldReturnBadRequestWhenCursorIsNotValidObjectId() throws Exception {
        // given
        final List<DrugRecordApplication> records = fixture.getMultipleDrugRecordApplication();
        mongoTemplate.insertAll(records);

        // when then
        mvc.perform(get("/api/storage/drug-record-application/by-cursor?cursor=InvalidObjectId&size=2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    private ObjectId getRecentCursor(List<DrugRecordApplication> drugRecordApplications) {
        return drugRecordApplications.stream()
                .map(DrugRecordApplication::getId).max(ObjectId::compareTo)
                .orElse(null);
    }

    private String getJsonBody(final DrugRecordApplication record) throws JsonProcessingException {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(record);
    }
}
