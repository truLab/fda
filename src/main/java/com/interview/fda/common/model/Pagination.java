package com.interview.fda.common.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Pagination {
    private final int page;
    private final int pageSize;
}
