package com.interview.fda.common.advice;

import com.interview.fda.search.exception.RequestNotHandledException;
import com.interview.fda.search.exception.ResultsNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@Slf4j
@RestControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler({ResultsNotFoundException.class})
    public ResponseEntity<?> handleResultsNotFoundException(final ResultsNotFoundException e) {
        log.error("Cannot find any results for provided query", e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
    }

    @ExceptionHandler({RequestNotHandledException.class})
    public ResponseEntity<?> handleRequestNotHandledException(final RequestNotHandledException e) {
        log.error("Request cannot be processed by FDA API at the moment", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler({DuplicateKeyException.class})
    public ResponseEntity<?> handleRequestNotHandledException(final DuplicateKeyException e) {
        log.error("Cannot add record. Already exists", e);
        return ResponseEntity.badRequest().body(e.getCause().getMessage());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<?> handleMethodArgumentNotValidException(final MethodArgumentNotValidException e) {
        log.error("Constraint violation exception for argument", e);
        final FieldError fieldError = e.getFieldError();
        if (fieldError != null) {
            return ResponseEntity.badRequest().body(fieldError.getDefaultMessage());
        } else {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<?> handleIllegalArgumentException(final IllegalArgumentException e) {
        log.error("Constraint violation exception for argument", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<?> handleConstraintViolationException(final ConstraintViolationException e) {
        log.error("Constraint violation exception for argument", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<?> handleHttpRequestMethodNotSupportedException(final HttpRequestMethodNotSupportedException e) {
        log.error("Requested not supported http method", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
