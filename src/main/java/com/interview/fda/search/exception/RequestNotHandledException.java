package com.interview.fda.search.exception;

import org.springframework.http.HttpStatus;

public class RequestNotHandledException extends RuntimeException {
    public RequestNotHandledException(final HttpStatus httpStatus) {
        super (String.format("Request not processed by FDA API. Response status code: %s", httpStatus.value()));
    }
}
