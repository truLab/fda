package com.interview.fda.search.controller;

import com.interview.fda.search.dto.controller.FdaDrugRecordApplicationResponse;
import com.interview.fda.common.model.Pagination;
import com.interview.fda.search.hateoas.FdaHateoasEnricher;
import com.interview.fda.search.model.SearchParameters;
import com.interview.fda.search.service.FdaDrugRecordApplicationSearchService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import static com.interview.fda.search.mapper.FdaSearchMapper.mapToFdaDrugRecordApplicationResponse;

@Slf4j
@RequiredArgsConstructor
@RequestMapping(value = "api/fda/", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@Validated
public class FdaSearchController {

    private final FdaDrugRecordApplicationSearchService fdaDrugRecordApplicationSearchService;
    private final FdaHateoasEnricher fdaHateoasEnricher;

    @Operation(description = "Search FDA API for drug record applications")
    @ApiResponse(responseCode = "400", description = "Wrong parameter requested")
    @ApiResponse(responseCode = "404", description = "Not found any drug record application")
    @GetMapping("drug-record-application")
    public ResponseEntity<FdaDrugRecordApplicationResponse> getDrugRecordApplications(
            @RequestParam("manufacturer_name") @NotBlank final String manufacturerName,
            @RequestParam(required = false, name = "brand_name") final String brandName,
            @RequestParam(defaultValue = "0") @Min(0) final int page,
            @RequestParam(defaultValue = "20", name = "page_size") @Min(1) @Max(50) final int pageSize) {

        log.debug("Search parameters values. Manufacturer name: '{}', brand name: '{}', page: '{}', pageSize '{}'",
                manufacturerName, brandName, page, pageSize);

        final SearchParameters parameters = SearchParameters.builder()
                .pagination(Pagination.builder()
                        .page(page)
                        .pageSize(pageSize)
                        .build())
                .manufacturerName(manufacturerName)
                .brandName(brandName)
                .build();

        final FdaDrugRecordApplicationResponse response =
                mapToFdaDrugRecordApplicationResponse(fdaDrugRecordApplicationSearchService.search(parameters));

        fdaHateoasEnricher.enrichResponseWithLinks(manufacturerName, brandName, page, pageSize, response);

        return ResponseEntity.ok(response);
    }

}
