package com.interview.fda.search.client;

import com.interview.fda.search.dto.client.FdaSearchResponse;
import com.interview.fda.search.exception.RequestNotHandledException;
import com.interview.fda.search.exception.ResultsNotFoundException;
import com.interview.fda.search.model.client.FdaSearchParameters;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@Component
public class FdaClient {

    private static final String BASE_FDA_URL = "https://api.fda.gov/drug/drugsfda.json";

    private final WebClient.Builder webClientBuilder;


    public FdaSearchResponse getDrugsFda(final FdaSearchParameters fdaSearchParameters) {

        final String searchUri = FdaUri.createUri(fdaSearchParameters);

        return webClientBuilder
                .baseUrl(BASE_FDA_URL)
                .build()
                .get()
                .uri(searchUri)
                .exchangeToMono(this::getFdaSearchResponse)
                .block();
    }

    private Mono<FdaSearchResponse> getFdaSearchResponse(final ClientResponse response) {
        if (response.statusCode().equals(HttpStatus.OK)) {
            return response.bodyToMono(FdaSearchResponse.class);
        } else if (response.statusCode().is4xxClientError()) {
            return Mono.error(new ResultsNotFoundException(response.statusCode()));
        } else {
            return Mono.error(new RequestNotHandledException(response.statusCode()));
        }
    }
}
