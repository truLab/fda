package com.interview.fda.search.client;

import com.interview.fda.search.model.client.FdaSearchParameters;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FdaUri {

    private static final String SEARCH_PREFIX = "?search=";
    private static final String MANUFACTURER_PARAM = "openfda.manufacturer_name";
    private static final String BRAND_PARAM = "openfda.brand_name";
    private static final String IS = ":";
    private static final String EQUALS = "=";
    private static final String AND = "&";

    public static String createUri(final FdaSearchParameters fdaSearchParameters) {

        return SEARCH_PREFIX
                .concat(resolveManufacturerName(fdaSearchParameters))
                .concat(resolveBrandName(fdaSearchParameters))
                .concat(resolveSkip(fdaSearchParameters))
                .concat(resolveLimit(fdaSearchParameters));
    }

    private static String resolveManufacturerName(final FdaSearchParameters fdaSearchParameters) {
        return MANUFACTURER_PARAM
                .concat(IS)
                .concat(fdaSearchParameters.getManufacturerName());
    }

    private static String resolveBrandName(final FdaSearchParameters fdaSearchParameters) {
        return fdaSearchParameters.getBrandName()
                .map(brandName ->
                        "+AND+"
                                .concat(BRAND_PARAM)
                                .concat(IS)
                                .concat(brandName)
                )
                .orElse("");
    }

    private static String resolveSkip(final FdaSearchParameters fdaSearchParameters) {
        return AND
                .concat("skip")
                .concat(EQUALS)
                .concat(String.valueOf(fdaSearchParameters.getSkip()));
    }

    private static String resolveLimit(final FdaSearchParameters fdaSearchParameters) {
        return AND
                .concat("limit")
                .concat(EQUALS)
                .concat(String.valueOf(fdaSearchParameters.getLimit()));
    }
}
