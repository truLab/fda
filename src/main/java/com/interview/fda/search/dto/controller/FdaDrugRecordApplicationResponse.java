package com.interview.fda.search.dto.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Builder
@Getter
public class FdaDrugRecordApplicationResponse extends RepresentationModel<FdaDrugRecordApplicationResponse> {
    @JsonProperty("number_of_results")
    private final long numberOfResults;

    @Singular
    @JsonProperty("fda_drug_results")
    private final List<FdaDrugResult> fdaDrugResults;
}
