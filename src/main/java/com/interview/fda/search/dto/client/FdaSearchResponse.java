package com.interview.fda.search.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class FdaSearchResponse {
    private FdaMeta meta;
    private List<FdaResult> results;

    public List<FdaResult> getResults() {
        return Collections.unmodifiableList(results);
    }
}
