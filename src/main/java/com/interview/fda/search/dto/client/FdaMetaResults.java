package com.interview.fda.search.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class FdaMetaResults {
    private long skip;
    private long limit;
    private long total;
}
