package com.interview.fda.search.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class OpenFda {
    @JsonProperty("application_number")
    private List<String> applicationNumbers;

    @JsonProperty("brand_name")
    private List<String> brandNames;

    @JsonProperty("generic_name")
    private List<String> genericNames;

    @JsonProperty("manufacturer_name")
    private List<String> manufacturerNames;

    public List<String> getApplicationNumbers() {
        return Collections.unmodifiableList(applicationNumbers);
    }

    public List<String> getBrandNames() {
        return Collections.unmodifiableList(brandNames);
    }

    public List<String> getGenericNames() {
        return Collections.unmodifiableList(genericNames);
    }

    public List<String> getManufacturerNames() {
        return Collections.unmodifiableList(manufacturerNames);
    }
}
