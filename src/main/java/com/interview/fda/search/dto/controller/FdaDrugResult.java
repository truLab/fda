package com.interview.fda.search.dto.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

import java.util.List;

@Builder
@Getter
public class FdaDrugResult {
    @JsonProperty("application_number")
    private final String applicationNumber;

    @JsonProperty("sponsor_name")
    private final String sponsorName;

    @Singular
    @JsonProperty("brand_name")
    private final List<String> brandNames;

    @Singular
    @JsonProperty("manufacturer_name")
    private final List<String> manufacturerNames;

    @Singular
    @JsonProperty("generic_name")
    private final List<String> genericNames;

    @Singular
    @JsonProperty("product_number")
    private final List<String> productNumbers;
}
