package com.interview.fda.search.dto.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class FdaResult {
    @JsonProperty("application_number")
    private String applicationNumber;

    @JsonProperty("sponsor_name")
    private String sponsorName;

    @JsonProperty("openfda")
    private OpenFda openFda;

    private List<FdaProduct> products;

    public List<FdaProduct> getProducts() {
        return Collections.unmodifiableList(products);
    }
}
