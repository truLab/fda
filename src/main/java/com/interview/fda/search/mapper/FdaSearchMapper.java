package com.interview.fda.search.mapper;

import com.interview.fda.search.dto.client.FdaProduct;
import com.interview.fda.search.dto.client.FdaSearchResponse;
import com.interview.fda.search.dto.controller.FdaDrugRecordApplicationResponse;
import com.interview.fda.search.dto.controller.FdaDrugResult;
import com.interview.fda.search.model.SearchParameters;
import com.interview.fda.search.model.client.FdaSearchParameters;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FdaSearchMapper {

    public static FdaDrugRecordApplicationResponse mapToFdaDrugRecordApplicationResponse(final FdaSearchResponse response) {
        return FdaDrugRecordApplicationResponse.builder()
                .numberOfResults(response.getMeta().getResults().getTotal())
                .fdaDrugResults(buildFdaDrugResults(response))
                .build();
    }

    public static FdaSearchParameters mapToFdaSearchParameters(SearchParameters parameters) {
        final long pageSize = parameters.getPagination().getPageSize();
        final long page = parameters.getPagination().getPage();

        return FdaSearchParameters.builder()
                .limit(pageSize)
                .skip(page * pageSize)
                .manufacturerName(parameters.getManufacturerName())
                .brandName(parameters.getBrandName())
                .build();
    }

    private static List<FdaDrugResult> buildFdaDrugResults(FdaSearchResponse response) {
        return response.getResults().stream().map(fdaResult ->
                FdaDrugResult.builder()
                        .applicationNumber(fdaResult.getApplicationNumber())
                        .sponsorName(fdaResult.getSponsorName())
                        .manufacturerNames(fdaResult.getOpenFda().getManufacturerNames())
                        .brandNames(fdaResult.getOpenFda().getBrandNames())
                        .genericNames(fdaResult.getOpenFda().getGenericNames())
                        .productNumbers(fdaResult.getProducts().stream()
                                .map(FdaProduct::getProductNumber)
                                .collect(Collectors.toUnmodifiableList()))
                        .build()
        ).collect(Collectors.toUnmodifiableList());
    }

}
