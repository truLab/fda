package com.interview.fda.search.service;

import com.interview.fda.search.client.FdaClient;
import com.interview.fda.search.dto.client.FdaSearchResponse;
import com.interview.fda.search.model.SearchParameters;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.interview.fda.search.mapper.FdaSearchMapper.mapToFdaSearchParameters;

@Service
@RequiredArgsConstructor
public class FdaDrugRecordApplicationSearchService {

    private final FdaClient fdaClient;

    public FdaSearchResponse search(final SearchParameters parameters) {
        return fdaClient.getDrugsFda(mapToFdaSearchParameters(parameters));
    }
}
