package com.interview.fda.search.hateoas;

import com.interview.fda.search.controller.FdaSearchController;
import com.interview.fda.search.dto.controller.FdaDrugRecordApplicationResponse;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class FdaHateoasEnricher {
    public void enrichResponseWithLinks(final String manufacturerName,
                                        final String brandName,
                                        final int page,
                                        final int pageSize,
                                        final FdaDrugRecordApplicationResponse response) {
        response.add(linkTo(methodOn(FdaSearchController.class)
                .getDrugRecordApplications(manufacturerName, brandName, page, pageSize)).withSelfRel());

        response.add(linkTo(methodOn(FdaSearchController.class)
                .getDrugRecordApplications(manufacturerName, brandName, page, pageSize)).withRel("nextPage"));

        if (hasPreviousPage(page)) {
            response.add(linkTo(methodOn(FdaSearchController.class)
                    .getDrugRecordApplications(manufacturerName, brandName, page, pageSize)).withRel("previousPage"));
        }
    }

    private boolean hasPreviousPage(int page) {
        return page > 0;
    }
}
