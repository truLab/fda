package com.interview.fda.search.model;

import com.interview.fda.common.model.Pagination;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class SearchParameters {
    private final Pagination pagination;
    private final String manufacturerName;
    private final String brandName;
}
