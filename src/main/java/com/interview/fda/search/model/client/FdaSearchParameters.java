package com.interview.fda.search.model.client;

import lombok.Builder;
import lombok.Getter;

import java.util.Optional;

@Builder
@Getter
public class FdaSearchParameters {
    private final long skip;
    private final long limit;
    private final String manufacturerName;
    private final String brandName;

    public Optional<String> getBrandName() {
        return Optional.ofNullable(brandName);
    }
}
