package com.interview.fda.storage.controller;

import com.interview.fda.common.model.Pagination;
import com.interview.fda.storage.dto.DrugRecordApplicationResponse;
import com.interview.fda.storage.dto.NewRecordRequest;
import com.interview.fda.storage.dto.NewRecordResponse;
import com.interview.fda.storage.dto.RecordWithCursorResponse;
import com.interview.fda.storage.dto.RecordWithPagingResponse;
import com.interview.fda.storage.hateoas.HateoasEnricher;
import com.interview.fda.storage.mapper.DrugRecordApplicationMapper;
import com.interview.fda.storage.model.Cursor;
import com.interview.fda.storage.model.DrugRecordApplication;
import com.interview.fda.storage.service.DrugRecordApplicationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Optional;

import static com.interview.fda.storage.mapper.DrugRecordApplicationMapper.mapToDrugRecordApplicationResponse;
import static com.interview.fda.storage.mapper.DrugRecordApplicationMapper.mapToRecordWithPagingResponse;

@Slf4j
@RequiredArgsConstructor
@RequestMapping(value = "api/storage/drug-record-application", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class DrugRecordApplicationController {

    private final DrugRecordApplicationService drugRecordApplicationService;
    private final HateoasEnricher hateoasEnricher;

    @Operation(description = "Store new drug record application")
    @ApiResponse(responseCode = "400", description = "Wrong parameter requested")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<NewRecordResponse> saveDrugRecordApplication(
            @Valid @RequestBody final NewRecordRequest newRecordRequest) {

        final DrugRecordApplication record =
                drugRecordApplicationService.save(DrugRecordApplicationMapper.mapToDrugRecordApplication(newRecordRequest));

        final NewRecordResponse response = DrugRecordApplicationMapper.mapToNewRecordResponse(record);

        hateoasEnricher.enrichNewRecord(response);

        return ResponseEntity.ok(response);
    }

    @Operation(description = "Find one drug record application in storage")
    @ApiResponse(responseCode = "400", description = "Wrong parameter requested")
    @ApiResponse(responseCode = "404", description = "Drug record application not found")
    @GetMapping("{applicationNumber}")
    public ResponseEntity<DrugRecordApplicationResponse> findByApplicationNumber(
            @PathVariable final String applicationNumber) {

        final Optional<DrugRecordApplication> drugRecordApplication =
                drugRecordApplicationService.findByApplicationNumber(applicationNumber);

        return drugRecordApplication.map(record -> {
            final DrugRecordApplicationResponse response =
                    mapToDrugRecordApplicationResponse(record);

            hateoasEnricher.enrichFoundDrugRecordApplication(applicationNumber, response);

            return ResponseEntity.ok(response);
        }).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Operation(description = "Find results for requested page")
    @ApiResponse(responseCode = "400", description = "Wrong parameter requested")
    @GetMapping("by-page")
    public ResponseEntity<RecordWithPagingResponse> findByPage(
            @RequestParam(defaultValue = "0") @Min(0) final int page,
            @RequestParam(defaultValue = "20" ,name = "page_size") @Min(1) @Max(50) final int pageSize) {

        final List<DrugRecordApplication> drugRecordApplications =
                drugRecordApplicationService.findByPage(Pagination.builder()
                        .page(page)
                        .pageSize(pageSize)
                        .build());

        final RecordWithPagingResponse response = mapToRecordWithPagingResponse(drugRecordApplications);

        hateoasEnricher.enrichFoundByPageRecord(page, pageSize, response);

        return ResponseEntity.ok(response);
    }

    @Operation(description = "Find results for requested cursor")
    @ApiResponse(responseCode = "400", description = "Wrong parameter requested")
    @GetMapping("by-cursor")
    public ResponseEntity<RecordWithCursorResponse> findByCursor(
            @RequestParam(required = false)
            @Parameter(description = "valid ObjectId", schema = @Schema(type="string")) final ObjectId cursor,
            @RequestParam(defaultValue = "20") @Min(1) @Max(50) final int size) {

        final List<DrugRecordApplication> drugRecordApplications =
                drugRecordApplicationService.findByCursor(Cursor.builder()
                        .cursor(cursor)
                        .size(size)
                        .build());

        final String recentCursor = getRecentCursor(drugRecordApplications);

        final RecordWithCursorResponse response =
                DrugRecordApplicationMapper.mapRecordWithCursorResponse(drugRecordApplications, recentCursor);

        hateoasEnricher.enrichFoundByCursorRecords(response, cursor, size, recentCursor);

        return ResponseEntity.ok(response);
    }

    private String getRecentCursor(List<DrugRecordApplication> drugRecordApplications) {
        return drugRecordApplications.stream()
                .map(DrugRecordApplication::getId).max(ObjectId::compareTo)
                .map(ObjectId::toHexString)
                .orElse("");
    }
}
