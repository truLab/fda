package com.interview.fda.storage.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Builder
@Getter
public class DrugRecordApplicationResponse extends RepresentationModel<DrugRecordApplicationResponse> {

    @JsonProperty("application_number")
    private final String applicationNumber;

    @JsonProperty("manufacturer_name")
    private final String manufacturerName;

    @JsonProperty("substance_name")
    private final String substanceName;

    @Singular
    @JsonProperty("product_numbers")
    private final List<String> productNumbers;
}
