package com.interview.fda.storage.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

@Builder
@Getter
public class NewRecordResponse extends RepresentationModel<NewRecordResponse> {

    @JsonProperty("application_number")
    private final String applicationNumber;

}
