package com.interview.fda.storage.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Builder
@Getter
public class RecordWithPagingResponse extends RepresentationModel<RecordWithPagingResponse> {

    @Singular
    private final List<DrugRecordApplicationResponse> records;
}
