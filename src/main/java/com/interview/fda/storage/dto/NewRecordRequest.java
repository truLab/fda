package com.interview.fda.storage.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@Getter
public class NewRecordRequest {

    @NotBlank(message = "application_number must be provided")
    @JsonProperty("application_number")
    private final String applicationNumber;

    @NotBlank(message = "manufacturer_name must be provided")
    @JsonProperty("manufacturer_name")
    private final String manufacturerName;

    @NotBlank(message = "substance_name must be provided")
    @JsonProperty("substance_name")
    private final String substanceName;

    @NotEmpty(message = "product_numbers must be provided")
    @JsonProperty("product_numbers")
    private final List<@NotBlank String> productNumbers;

    public List<String> getProductNumbers() {
        return Collections.unmodifiableList(productNumbers);
    }
}
