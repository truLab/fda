package com.interview.fda.storage.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Builder
@Getter
public class RecordWithCursorResponse extends RepresentationModel<RecordWithCursorResponse> {
    private final String cursor;

    @Singular
    private final List<DrugRecordApplicationResponse> records;
}
