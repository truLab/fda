package com.interview.fda.storage.repository;

import com.interview.fda.common.model.Pagination;
import com.interview.fda.storage.model.Cursor;
import com.interview.fda.storage.model.DrugRecordApplication;
import lombok.RequiredArgsConstructor;
import org.bson.BsonMinKey;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.interview.fda.storage.model.DrugRecordApplication.APPLICATION_NUMBER;
import static com.interview.fda.storage.model.DrugRecordApplication.DEFAULT_ID;


@RequiredArgsConstructor
@Repository
public class DrugRecordApplicationStorageRepository implements DrugRecordApplicationRepository {

    private final MongoTemplate mongoTemplate;

    @Override
    public DrugRecordApplication save(final DrugRecordApplication drugRecordApplication) {
        return mongoTemplate.save(drugRecordApplication);
    }

    @Override
    public Optional<DrugRecordApplication> findByApplicationNumber(final String applicationNumber) {

        final Criteria criteria = Criteria
                .where(APPLICATION_NUMBER)
                .is(applicationNumber);

        return Optional.ofNullable(mongoTemplate.findOne(Query.query(criteria), DrugRecordApplication.class));
    }

    @Override
    public List<DrugRecordApplication> findByPage(final Pagination pagination) {

        Pageable pageable = PageRequest.of(pagination.getPage(), pagination.getPageSize());

        return mongoTemplate.find(new Query().with(pageable), DrugRecordApplication.class);
    }

    @Override
    public List<DrugRecordApplication> findByCursor(final Cursor cursor) {
        final Criteria criteria = new Criteria()
                .and(DEFAULT_ID).gt(cursor.getCursor().isEmpty() ? new BsonMinKey() : cursor.getCursor().get());

        final Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, DEFAULT_ID));

        return mongoTemplate.find(
                Query.query(criteria).limit(cursor.getSize()).with(sort),
                DrugRecordApplication.class);
    }

}
