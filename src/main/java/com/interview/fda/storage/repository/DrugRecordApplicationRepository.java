package com.interview.fda.storage.repository;

import com.interview.fda.common.model.Pagination;
import com.interview.fda.storage.model.Cursor;
import com.interview.fda.storage.model.DrugRecordApplication;

import java.util.List;
import java.util.Optional;

public interface DrugRecordApplicationRepository {
    DrugRecordApplication save(DrugRecordApplication drugRecordApplication);

    Optional<DrugRecordApplication> findByApplicationNumber(final String applicationNumber);

    List<DrugRecordApplication> findByPage(final Pagination pagination);

    List<DrugRecordApplication> findByCursor(final Cursor cursor);
}
