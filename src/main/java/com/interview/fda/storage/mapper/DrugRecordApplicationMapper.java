package com.interview.fda.storage.mapper;

import com.interview.fda.storage.dto.DrugRecordApplicationResponse;
import com.interview.fda.storage.dto.NewRecordRequest;
import com.interview.fda.storage.dto.NewRecordResponse;
import com.interview.fda.storage.dto.RecordWithCursorResponse;
import com.interview.fda.storage.dto.RecordWithPagingResponse;
import com.interview.fda.storage.model.DrugRecordApplication;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DrugRecordApplicationMapper {

    public static NewRecordResponse mapToNewRecordResponse(final DrugRecordApplication drugRecordApplication) {
        return NewRecordResponse.builder()
                .applicationNumber(drugRecordApplication.getApplicationNumber())
                .build();
    }

    public static DrugRecordApplication mapToDrugRecordApplication(final NewRecordRequest newRecordRequest) {
        return DrugRecordApplication.builder()
                .applicationNumber(newRecordRequest.getApplicationNumber())
                .manufacturerName(newRecordRequest.getManufacturerName())
                .substanceName(newRecordRequest.getSubstanceName())
                .productNumbers(newRecordRequest.getProductNumbers())
                .build();
    }

    public static DrugRecordApplicationResponse mapToDrugRecordApplicationResponse(
            final DrugRecordApplication drugRecordApplication) {

        return DrugRecordApplicationResponse.builder()
                .applicationNumber(drugRecordApplication.getApplicationNumber())
                .manufacturerName(drugRecordApplication.getManufacturerName())
                .substanceName(drugRecordApplication.getSubstanceName())
                .productNumbers(drugRecordApplication.getProductNumbers())
                .build();
    }

    public static RecordWithCursorResponse mapRecordWithCursorResponse(
            final List<DrugRecordApplication> drugRecordApplications,
            final String recentCursor) {

        return RecordWithCursorResponse.builder()
                .cursor(recentCursor)
                .records(mapRecordsToRecordsResponse(drugRecordApplications))
                .build();
    }

    public static RecordWithPagingResponse mapToRecordWithPagingResponse(
            final List<DrugRecordApplication> drugRecordApplications) {

        return RecordWithPagingResponse.builder()
                .records(mapRecordsToRecordsResponse(drugRecordApplications))
                .build();
    }

    private static List<DrugRecordApplicationResponse> mapRecordsToRecordsResponse(
            final List<DrugRecordApplication> drugRecordApplications) {

        return drugRecordApplications.stream()
                .map(DrugRecordApplicationMapper::mapToDrugRecordApplicationResponse)
                .collect(Collectors.toUnmodifiableList());
    }

}
