package com.interview.fda.storage.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Getter
@Builder
@Document
public class DrugRecordApplication {

    public static final String DEFAULT_ID = "_id";
    public static final String APPLICATION_NUMBER = "applicationNumber";
    public static final String MANUFACTURER_NAME = "manufacturerName";
    public static final String SUBSTANCE_NAME = "substanceName";
    public static final String PRODUCT_NUMBERS = "productNumbers";

    @Id
    @Field(value = DEFAULT_ID)
    private final ObjectId id;

    @Indexed(unique=true)
    @Field(value = APPLICATION_NUMBER)
    private final String applicationNumber;

    @Field(value = MANUFACTURER_NAME)
    private final String manufacturerName;

    @Field(value = SUBSTANCE_NAME)
    private final String substanceName;

    @Singular
    @Field(value = PRODUCT_NUMBERS)
    private final List<String> productNumbers;
}
