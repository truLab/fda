package com.interview.fda.storage.model;

import lombok.Builder;
import lombok.Getter;
import org.bson.types.ObjectId;

import java.util.Optional;

@Getter
@Builder
public class Cursor {
    private final ObjectId cursor;
    private final int size;

    public Optional<ObjectId> getCursor() {
        return Optional.ofNullable(cursor);
    }
}
