package com.interview.fda.storage.service;

import com.interview.fda.common.model.Pagination;
import com.interview.fda.storage.model.Cursor;
import com.interview.fda.storage.repository.DrugRecordApplicationRepository;
import com.interview.fda.storage.model.DrugRecordApplication;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DrugRecordApplicationService {

    private final DrugRecordApplicationRepository drugRecordApplicationRepository;

    public DrugRecordApplication save(final DrugRecordApplication drugRecordApplication) {
        return drugRecordApplicationRepository.save(drugRecordApplication);
    }

    public Optional<DrugRecordApplication> findByApplicationNumber(final String applicationNumber) {
        return drugRecordApplicationRepository.findByApplicationNumber(applicationNumber);
    }

    public List<DrugRecordApplication> findByPage(final Pagination pagination) {
        return drugRecordApplicationRepository.findByPage(pagination);
    }

    public List<DrugRecordApplication> findByCursor(final Cursor cursor) {
        return drugRecordApplicationRepository.findByCursor(cursor);
    }
}
