package com.interview.fda.storage.hateoas;

import com.interview.fda.storage.controller.DrugRecordApplicationController;
import com.interview.fda.storage.dto.DrugRecordApplicationResponse;
import com.interview.fda.storage.dto.NewRecordResponse;
import com.interview.fda.storage.dto.RecordWithCursorResponse;
import com.interview.fda.storage.dto.RecordWithPagingResponse;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class HateoasEnricher {

    public void enrichNewRecord(final NewRecordResponse response) {
        response.add(linkTo(methodOn(DrugRecordApplicationController.class).
                findByApplicationNumber(response.getApplicationNumber())).withRel("findAddedRecord"));
    }

    public void enrichFoundDrugRecordApplication(final String applicationNumber,
                                                 final DrugRecordApplicationResponse response) {
        response.add(linkTo(methodOn(DrugRecordApplicationController.class)
                .findByApplicationNumber(applicationNumber)).withSelfRel());
    }

    public void enrichFoundByPageRecord(final int page,
                                        final int pageSize,
                                        final RecordWithPagingResponse response) {
        response.add(linkTo(methodOn(DrugRecordApplicationController.class)
                .findByPage(page, pageSize)).withSelfRel());

        response.add(linkTo(methodOn(DrugRecordApplicationController.class)
                .findByPage(page + 1, pageSize)).withRel("nextPage"));

        if (hasPreviousPage(page)) {
            response.add(linkTo(methodOn(DrugRecordApplicationController.class)
                    .findByPage(page - 1, pageSize)).withRel("previousPage"));
        }
    }

    public void enrichFoundByCursorRecords(final RecordWithCursorResponse response,
                                           final ObjectId cursor,
                                           final int size,
                                           final String recentCursor) {
        response.add(linkTo(methodOn(DrugRecordApplicationController.class)
                .findByCursor(cursor, size)).withSelfRel());

        if (hasNextPage(recentCursor)) {
            response.add(linkTo(methodOn(DrugRecordApplicationController.class)
                    .findByCursor(new ObjectId(recentCursor), size)).withRel("nextPage"));
        }
    }

    private boolean hasNextPage(String recentCursor) {
        return !recentCursor.isBlank();
    }

    private boolean hasPreviousPage(final int page) {
        return page > 0;
    }

}
