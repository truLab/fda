package com.interview.fda;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
		info = @Info(
				title = "Drug Record Applications",
				version = "1.0",
				description = "FDA search support and local storage of drug record applications"
		)
)
@SpringBootApplication
public class FdaApplication {

	public static void main(String[] args) {
		SpringApplication.run(FdaApplication.class, args);
	}

}
