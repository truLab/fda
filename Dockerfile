FROM maven:3.6.3-jdk-11-slim AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package -Dmaven.test.skip=true

FROM openjdk:11-jdk-slim
VOLUME /tmp
EXPOSE 8080
COPY --from=build /usr/src/app/target/fda-0.0.1-SNAPSHOT.jar fda-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar","fda-0.0.1-SNAPSHOT.jar"]
